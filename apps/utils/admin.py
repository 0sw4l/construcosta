from django.contrib import admin

# Register your models here.
base_list_user = ['username', 'get_full_name',
                    'telefono_fijo', 'telefono_celular',
                    'tipo_documento', 'identificacion', 'direccion']


base_list_item_selling = ['nombre', 'categoria', 'valor', 'proveedor']

base_name = ['nombre']

