from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^inicio/',
        views.IndexTemplateView.as_view(),
        name='inicio')
]
