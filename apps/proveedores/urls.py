from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^crear-proveedor/',
        views.ProveedorCreateView.as_view(),
        name='crear_proveedor'),

    url(r'^crear-producto/',
        views.ProductoProveedorCreateView.as_view(),
        name='crear_producto'),

    url(r'^crear-servicio/',
        views.ServicioProveedorCreateView.as_view(),
        name='crear_servicio'),

    url(r'^lista-productos/',
        views.ProductoProveedorListView.as_view(),
        name='lista_productos'),

    url(r'^lista-servicios/',
        views.ServicioProveedorListView.as_view(),
        name='lista_servicios')
]
