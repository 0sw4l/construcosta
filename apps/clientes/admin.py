from django.contrib import admin

from apps.utils.admin import base_list_user
from .models import Cliente
# Register your models here.


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = base_list_user

