from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.views.generic import CreateView

from apps.proveedores.forms import ProveedorForm, ProductoProovedorForm, ServicioProveedorForm
from apps.utils.views import BaseCreateView, BaseListView
from .models import ServicioProveedor, ProductoProveedor, Proveedor
# Create your views here.


class ProveedorCreateView(CreateView):
    form_class = ProveedorForm
    template_name = 'forms/with_login.html'
    success_url = reverse_lazy('entrar')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Crear'
        return context


class ProductoProveedorCreateView(BaseCreateView):
    form_class = ProductoProovedorForm


class ProductoProveedorListView(BaseListView):
    model = ProductoProveedor
    template_name = 'apps/proveedores/lista_productos.html'


class ServicioProveedorCreateView(BaseCreateView):
    form_class = ServicioProveedorForm


class ServicioProveedorListView(BaseListView):
    model = ServicioProveedor
    template_name = 'apps/proveedores/lista_servicios.html'

