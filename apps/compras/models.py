from django.db import models

# Create your models here.
from apps.clientes.models import Cliente
from apps.proveedores.models import Proveedor, ProductoProveedor, ServicioProveedor


class CompraProducto(models.Model):
    cliente = models.ForeignKey(Cliente, related_name='+')
    proveedor = models.ForeignKey(Proveedor, related_name='+')

    def total(self):
        ganancia = 0
        for compra in CompraProductoItem.objects.filter(compra=self):
            ganancia += compra.total
        return ganancia


class CompraProductoItem(models.Model):
    compra = models.ForeignKey(CompraProducto, related_name='compra_producto_item')
    producto = models.ForeignKey(ProductoProveedor)
    cantidad = models.PositiveIntegerField()
    total = models.PositiveIntegerField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.total = self.cantidad * self.producto.valor
        super().save(*args, **kwargs)


class CompraServicio(models.Model):
    cliente = models.ForeignKey(Cliente, related_name='+')
    proveedor = models.ForeignKey(Proveedor, related_name='+')

    def total(self):
        ganancia = 0
        for compra in CompraServicio.objects.filter(compra=self):
            ganancia += compra.total
        return ganancia


class CompraServicioItem(models.Model):
    compra = models.ForeignKey(CompraServicio, related_name='compra_servicio_item')
    producto = models.ForeignKey(ProductoProveedor)
    cantidad = models.PositiveIntegerField()
    total = models.PositiveIntegerField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.total = self.cantidad * self.producto.valor
        super().save(*args, **kwargs)
