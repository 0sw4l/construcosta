from django.contrib import admin

from apps.utils.admin import base_list_user, base_list_item_selling, base_name
from .models import Proveedor, ProductoProveedor, CategoriaProducto, CategoriaServicio, ServicioProveedor
# Register your models here.


@admin.register(Proveedor)
class ProveedorAdmin(admin.ModelAdmin):
    list_display = base_list_user


@admin.register(ProductoProveedor)
class ProductoProveedorAdmin(admin.ModelAdmin):
    list_display = base_list_item_selling


@admin.register(ServicioProveedor)
class ServicioProveedorAdmin(admin.ModelAdmin):
    list_display = base_list_item_selling


@admin.register(CategoriaProducto)
class CategoriaProductoAdmin(admin.ModelAdmin):
    list_display = base_name


@admin.register(CategoriaServicio)
class CategoriaServicioAdmin(admin.ModelAdmin):
    list_display = base_name



