from django.db import models

# Create your models here.
from apps.main.models import BaseName
from apps.utils.models import UserBase, VentaItem


class Proveedor(UserBase):
    class Meta:
        verbose_name = 'Proveedor'
        verbose_name_plural = 'Proveedores'


class CategoriaProducto(BaseName):
    pass


class CategoriaServicio(BaseName):
    pass


class ProductoProveedor(VentaItem):
    categoria = models.ForeignKey(CategoriaProducto)


class ServicioProveedor(VentaItem):
    categoria = models.ForeignKey(CategoriaServicio)


