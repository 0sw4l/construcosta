from django import forms
from django.contrib.auth.forms import UserCreationForm


class BaseForm(forms.ModelForm):
    title = ''

    class Meta:
        model = None
        fields = '__all__'


class BaseUserForm(UserCreationForm):
    title = ''

    class Meta:
        model = None
        exclude = (
            'last_login',
            'first_name',
            'last_name',
            'is_staff',
            'is_superuser',
            'date_joined',
            'groups',
            'user_permissions',
            'password',
            'is_active',
            'email',
        )
        fields = '__all__'
