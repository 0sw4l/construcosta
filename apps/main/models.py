from django.db import models


class BaseName(models.Model):
    nombre = models.CharField(max_length=255, unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return '{0}'.format(self.nombre)
