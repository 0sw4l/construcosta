from django import forms

from apps.utils.forms import BaseUserForm
from .models import Cliente


class ClienteForm(BaseUserForm):
    title = 'Cliente'

    class Meta(BaseUserForm.Meta):
        model = Cliente


