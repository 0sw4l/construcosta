from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from apps.main.models import BaseName


class UserBase(User):
    telefono_fijo = models.PositiveIntegerField()
    telefono_celular = models.PositiveIntegerField()
    direccion = models.CharField(max_length=150)
    TIPOS = (
        ('NIT', 'NIT'),
        ('RUT', 'RUT'),
        ('CC', 'CC')
    )
    tipo_documento = models.CharField(
        max_length=5,
        choices=TIPOS
    )
    identificacion = models.CharField(
        unique=True,
        max_length=20
    )

    class Meta:
        abstract = True


class VentaItem(BaseName):
    proveedor = models.ForeignKey('proveedores.Proveedor')
    valor = models.PositiveIntegerField()
