from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render

# Create your views here.
from django.views.generic import CreateView

from apps.clientes.forms import ClienteForm
from apps.clientes.models import Cliente


class ClienteCreateView(CreateView):
    template_name = 'forms/with_login.html'
    form_class = ClienteForm
    success_url = reverse_lazy('entrar')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Crear'
        return context


