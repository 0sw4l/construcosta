from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^crear-cliente/',
        views.ClienteCreateView.as_view(),
        name='crear_cliente')
]
