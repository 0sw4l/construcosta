from django import forms

from apps.utils.forms import BaseForm, BaseUserForm
from .models import Proveedor, CategoriaProducto, CategoriaServicio, ProductoProveedor, ServicioProveedor


class ProveedorForm(BaseUserForm):
    title = 'Proveedor'

    class Meta(BaseUserForm.Meta):
        model = Proveedor


class CategoriaProductoForm(BaseForm):
    title = 'Categoria Producto'

    class Meta(BaseForm.Meta):
        model = CategoriaProducto


class CategoriaServicioForm(BaseForm):
    title = 'Categoria Servicio'

    class Meta(BaseForm.Meta):
        model = CategoriaServicio


class ProductoProovedorForm(BaseForm):
    title = 'Producto Proveedor'

    class Meta(BaseForm.Meta):
        model = ProductoProveedor


class ServicioProveedorForm(BaseForm):
    title = 'Servicio Proveedor'

    class Meta(BaseForm.Meta):
        model = ServicioProveedor

